<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ContactDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('contact_details', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('sub_service_id');
        $table->integer('service_type_id');
        $table->string('details_number');
        $table->string('other')->default('');
        $table->softDeletes();
        $table->timestamps();
    });
   }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contact_details');

    }
}
