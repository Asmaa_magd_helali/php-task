<?php

use Illuminate\Database\Seeder;
use App\Service;
use App\SubService;
use App\ServiceType;




class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $create=Service::create([
    		'title_ar'  =>'تصوير فوتوغرافى',
    		'title_en'  =>'Photography',
    		]);
       SubService::create([
    		'title_ar'  =>' أطعمة ومشروبات',
    		'title_en'  =>' Food & Drink',
    		'service_id'=>$create->id,
    		]);
 SubService::create([
    		'title_ar'  =>'اثاث',
    		'title_en'  =>' Furniture',
    		'service_id'=>$create->id,
    		]);
  SubService::create([
    		'title_ar'  =>'فنادق',
    		'title_en'  =>'Hotels',
    		'service_id'=>$create->id,
    		]);
   SubService::create([
    		'title_ar'  =>'عقار',
    		'title_en'  =>' Realty',
    		'service_id'=>$create->id,
    		]);
    SubService::create([
    		'title_ar'  =>'اجهزة الكترونية',
    		'title_en'  =>'Electronic devices',
    		'service_id'=>$create->id,
    		]);
     SubService::create([
    		'title_ar'  =>'اشخاص',
    		'title_en'  =>'Portrait',
    		'service_id'=>$create->id,
    		]);
      SubService::create([
    		'title_ar'  =>'توثيق مناسبات',
    		'title_en'  =>' Documenting Events',
    		'service_id'=>$create->id,
    		]);
       ServiceType::create([
    		'title_ar'  =>'خلفية مفرغة',
    		'title_en'  =>'Without Background',
    		'service_id'=>$create->id,
    		]);
       ServiceType::create([
    		'title_ar'  =>'خلفية مركبة',
    		'title_en'  =>' With Artistic Background',
    		'service_id'=>$create->id,
    		]);
       ServiceType::create([
    		'title_ar'  =>' تصوير بالشكل الطبيعي',
    		'title_en'  =>' Normal Imaging',
    		'service_id'=>$create->id,
    		]);
       ServiceType::create([
    		'title_ar'  =>' الممثلين ( تصوير الأشخاص)',
    		'title_en'  =>' Models (Portrait)',
    		'service_id'=>$create->id,
    		]);
     $create=  Service::create([
    		'title_ar'  =>'تصوير فيديو',
    		'title_en'  =>'video',
    		]);
     SubService::create([
    		'title_ar'  =>'فيلم تعريفي',
    		'title_en'  =>'Definitions Film',
    		'service_id'=>$create->id,
    		]);
     SubService::create([
    		'title_ar'  =>'فيلم وثائقى',
    		'title_en'  =>'Documentary Film',
    		'service_id'=>$create->id,
    		]);
     SubService::create([
    		'title_ar'  =>'فيلم تخرج',
    		'title_en'  =>'Graduated Film',
    		'service_id'=>$create->id,
    		]);
     SubService::create([
    		'title_ar'  =>' إعلان تلفزيوني',
    		'title_en'  =>'Television Advertisement',
    		'service_id'=>$create->id,
    		]);
     SubService::create([
    		'title_ar'  =>'الرسوم البيانية',
    		'title_en'  =>'Infographics',
    		'service_id'=>$create->id,
    		]);
     SubService::create([
    		'title_ar'  =>'توثيق مناسبات',
    		'title_en'  =>'Documenting Events',
    		'service_id'=>$create->id,
    		]);
      ServiceType::create([
    		'title_ar'  =>'كتابة السيناريو',
    		'title_en'  =>' Scenario',
    		'service_id'=>$create->id,
    		]);
       ServiceType::create([
    		'title_ar'  =>'مواقع التصوير',
    		'title_en'  =>'Locations',
    		'service_id'=>$create->id,
    		]);
        ServiceType::create([
    		'title_ar'  =>'الممثلين',
    		'title_en'  =>'Models',
    		'service_id'=>$create->id,
    		]);
      $create= Service::create([
    		'title_ar'  =>'تصميم جرافيك',
    		'title_en'  =>'Graphic Design',
    		]);
      SubService::create([
    		'title_ar'  =>'تصميم كتيب',
    		'title_en'  =>' Book Design',
    		'service_id'=>$create->id,
    		]);
      SubService::create([
    		'title_ar'  =>' تصميم بوستر',
    		'title_en'  =>'Poster Design',
    		'service_id'=>$create->id,
    		]);
      SubService::create([
    		'title_ar'  =>'تصميم برشور',
    		'title_en'  =>'Brochure Design',
    		'service_id'=>$create->id,
    		]);
      SubService::create([
    		'title_ar'  =>'تصميم نشرة اعلانية',
    		'title_en'  =>'Flyer Design',
    		'service_id'=>$create->id,
    		]);
      SubService::create([
    		'title_ar'  =>'تصميم قائمة طعام',
    		'title_en'  =>'Menu Design',
    		'service_id'=>$create->id,
    		]);
      SubService::create([
    		'title_ar'  =>'تصميمات اعلانات ترويجية',
    		'title_en'  =>' Promotional Advertising Design',
    		'service_id'=>$create->id,
    		]);
    }
}
