<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['middleware' => 'web','prefix' => 'admin', 'namespace'=>'admin' , 'as'=>'admin.'  ], function () {
    //    Route::get('/link1', function ()    {
//        // Uses Auth Middleware
//    });

    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
    #adminlte_routes
    Route::get('/adminpanel', function () {
	return view('welcome');});
	 Route::auth();
	Route::resource('submember' , 'SubscribeMembersController');
    Route::get('/home', 'HomeController@index');




});
Route::group(['prefix' => 'site', 'namespace'=>'site' , 'as'=>'site.' ], function () {
	Route::resource('home' , 'HomeController');
	Route::resource('contactus' , 'ContactUsController');
	Route::post('joinus','ContactUsController@joinUs');


	Route::get('change/language/{lang}', ['as' => 'lang.switch', 'uses' => 'HomeController@changeLanguage']);
	Route::get('aboutus', function () {
		return view('site.about');
	});
	Route::get('services', function () {
		return view('site.services');
	});
	Route::get('gallery', function () {
		return view('site.gallery');
	});
	Route::get('category', function () {
		return view('site.category');
	});



});	
