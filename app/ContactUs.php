<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ContactUs extends Model
{
   use SoftDeletes;
	protected $table='contactus';

	protected $fillable = [
	'company_name','activity_type','telephone','contact_email','attachment'
	];
	protected $dates = ['deleted_at'];

 
}
