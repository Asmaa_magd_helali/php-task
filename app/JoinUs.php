<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class JoinUs extends Model
{
	use SoftDeletes;
	protected $table='join_us';

	protected $fillable = [
	'email'
	];
	protected $dates = ['deleted_at'];

}
