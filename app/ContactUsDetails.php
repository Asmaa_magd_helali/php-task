<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ContactUsDetails extends Model
{
      use SoftDeletes;
	protected $table='contact_details';

	protected $fillable = [
	'contact_id','sub_service_id','service_type_id','details_number'
	];
	protected $dates = ['deleted_at'];
}
