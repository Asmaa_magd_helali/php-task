<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceType extends Model
{
   use SoftDeletes;
	protected $table='service_type';

	protected $fillable = [
	'title_ar','title_en',' 	service_id'
	];
	protected $dates = ['deleted_at'];
}
