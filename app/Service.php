<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Service extends Model
{
   use SoftDeletes;
	protected $table='service';

	protected $fillable = [
	'title_ar','title_en'
	];
	protected $dates = ['deleted_at'];

	  public function subServices()
    {
        return $this->hasMany('App\SubService');
    }
      public function serviceType()
    {
        return $this->hasMany('App\ServiceType');
    }
}
