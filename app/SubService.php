<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class SubService extends Model
{
     use SoftDeletes;
	protected $table='sub_service';

	protected $fillable = [
	'title_ar','title_en','service_id'
	];
	protected $dates = ['deleted_at'];
}
