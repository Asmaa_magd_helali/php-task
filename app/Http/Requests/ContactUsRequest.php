<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactUsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        'company_name'       =>'required',
        'activity_type'       =>'required',
        'telephone'       =>'required|numeric',
        'contact_email'       =>'required|email',
        'attachment'       =>'required|file',

        ];
    }
}
