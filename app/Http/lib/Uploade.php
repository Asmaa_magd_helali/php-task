<?php

namespace App\Http\lib;


class Uploade
{

    private static $_instance;
    public static function getInstance()
    {
        if (!(self::$_instance instanceof self))
        {
            self::$_instance = new self();
        }
     
        return self::$_instance;
    }

    public function uploadeFile($type, $file)
    {
        try {
            switch ($type) {
                case '0':
                $folder = "files/";
                break;
                default:
                $folder = "/";
                break;
            }
            $file_name = substr(md5(time()), 0, 15);
            $extension = $file->getClientOriginalExtension();
            $fileName = $file_name . "." . $extension;
            $destinationPath = public_path("uploadedfiles/" . $folder);
            $file->move($destinationPath, $fileName);
            return $fileName;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }


}


