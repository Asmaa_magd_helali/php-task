<?php

namespace App\Http\Controllers\site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\JoinUsRequest;
use App\Http\Requests\ContactUsRequest;
use App\JoinUs;
use App\Service;
use App\ContactUs;
use App\Http\lib\Uploade;


class ContactUsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     $data['services'] = Service::all();
     $data['lang']=\App::getLocale();
     return view('site.contact' ,$data);

 }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContactUsRequest $request)
    {
        // dd($request);
        $attachment  = Uploade::getInstance()->uploadeFile("0",$request->attachment);
        $contact=ContactUs::create(["company_name"=>$request->company_name,"activity_type"=>$request->activity_type,"telephone"=>$request->telephone,"contact_email"=>$request->contact_email,"attachment"=>$attachment]);
        return redirect('site/home')->with('success' , trans('layout.add_succ'));


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function joinUs(JoinUsRequest $request){

        JoinUs::create(['email' => $request->email]);
        return redirect('site/home')->with('success' , trans('layout.add_succ'));
    }
}
