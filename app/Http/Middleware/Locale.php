<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class Locale
{
    public function handle($request, Closure $next)
    {
    	if($request->is('admin/*')){
			App::setLocale('ar');
    	}
    	else{
    		 if (Session::has('applocale') AND in_array(Session::get('applocale') , ['ar' , 'en']))
            App::setLocale(Session::get('applocale'));
        else
            App::setLocale(Config::get('app.fallback_locale'));
    	}
    	
       

        return $next($request);
    }
}
