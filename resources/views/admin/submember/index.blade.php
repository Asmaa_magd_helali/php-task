@extends('adminlte::layouts.app')

@section('contentheader_title')
	{{trans('layout.submembers')}}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				   <div class="withScroll" data-height="400">
                                            <table class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th class="text-center">{{trans('layout.email')}}</th>
                                                    <th class="text-center">{{trans('layout.created_at')}}</th>
                                                    <th class="text-center">{{trans('layout.delete')}}</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @forelse($members as $member)
                                                        <tr>
                                                           
                                                            <td class="text-center">{{$member->email}}</td>
                                                            <td class="text-center">{{$member->created_at}}</td>
                                                            <td class="text-center">
                                                                {{Form::open(['route'=>['admin.submember.update' , $member->id ] , 'method'=>'delete' , 'id'=>'form'])}}
                                                                <a href="javascript:;" onclick="if(confirm('{{trans('layout.deleteOp')}}')) $(this).closest('form').submit();"> <i class="fa fa-trash" aria-hidden="true" style="color:rgb(240, 80, 80)"></i></a>
                                                                {{Form::close()}}
                                                            </td>
                                                        </tr>
                                                    @empty
                                                    <tr>
                                                        <td colspan="4">
                                                            <div class="alert alert-danger text-center">{{trans('layout.noData')}}</div>
                                                        </td>
                                                    </tr>
                                                @endforelse
                                                </tbody>
                                            </table>
                                        </div>

			</div>
		</div>
	</div>
@endsection
