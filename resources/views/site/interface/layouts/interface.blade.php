<?php
$lang = \App::getLocale();
?>
<!DOCTYPE html>
<html lang="{{$lang}}">
{{-- <header>  --}}
@include('site.interface.layouts.head')
{{-- </header> --}}
<body >
   

	<!-- ========== Header ========== -->
	@include('site.interface.layouts.header')
	<!-- ========== Header End ========== -->

	{{-- content --}}
	@yield('content') 
	{{-- END  content --}}

	<!--FOOTER -->
	@include('site.interface.layouts.footer')
	
	<!-- END FOOTER -->
</body>
</html>

