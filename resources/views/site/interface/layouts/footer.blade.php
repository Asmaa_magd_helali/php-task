<footer class="text-center">
    <div class="container">

        <p>{{trans('layout.copyright')}}  &copy; 2005-2015 </p>

        <a href="#"><i class="fa fa-facebook"></i></a>
        <a href="#"><i class="fa fa-twitter"></i></a>
        <a href="#"><i class="fa fa-google-plus"></i></a>
        <a href="#"><i class="fa fa-instagram"></i></a>
        <a href="#"><i class="fa fa-youtube"></i></a>
        <a href="#"><i class="fa fa-pinterest"></i></a>
        <a href="#"><i class="fa fa-behance"></i></a>
        <a href="#"><i class="fa fa-vimeo"></i></a>

    </div>
</footer>
<script src="{{URL::to('/')}}/admin/assets/js/jquery-1.11.1.min.js"></script>
<script src="{{URL::to('/')}}/admin/assets/js/bootstrap.min.js"></script>
<script src="{{URL::to('/')}}/admin/assets/owl-carousel/owl.carousel.min.js"></script>

<script src="{{URL::to('/')}}/admin/assets/js/script.js"></script>
@yield('aboutus_js')
@yield('category_js')
@yield('contact_footer_js')


