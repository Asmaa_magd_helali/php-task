<nav class="navbar navbar-fixed-top">
    <div class="container">
        <div class="row">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
                    <span class="fa fa-bars"></span>
                </button>

                <a href="{{url('site/home')}}" class="navbar-brand hidden-sm hidden-md hidden-lg"><img src="{{URL::to('/')}}/admin/assets/images/logo.png" alt="LOGO"></a>
            </div>

            <div class="collapse navbar-collapse" id="navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right text-align-left">
                    <li class="active"><a href="{{url('/site/home')}}">{{trans('layout.home')}}</a></li>
                    <li><a href="{{url('site/aboutus')}}">{{trans('layout.aboutus')}}</a></li>
                    <li><a href="{{url('site/services')}}">{{trans('layout.ourservices')}}</a></li>
                </ul>

                <a href="{{url('site/home')}}" class="navbar-brand hidden-xs text-center"><img src="{{URL::to('/')}}/admin/assets/images/logo.png" alt="LOGO"></a>

                <ul class="nav navbar-nav navbar-left text-align-right">
                    <li><a href="{{url('site/gallery')}}">{{trans('layout.gallery')}}</a></li>
                    <li><a href="{{url('site/contactus')}}">{{trans('layout.contactus')}}</a></li>
                    @if($lang == 'ar')

                    <li><a href="{{url('site/change/language/en')}}">English</a></li>
                    @else
                    <li><a href="{{url('site/change/language/ar')}}">عربى</a></li>

                    @endif
                </ul>
            </div>
        </div>
    </div>
</nav>