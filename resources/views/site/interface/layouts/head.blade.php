<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
       <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="HandheldFriendly" content="true">
    <title>Photo Maker</title>
    <link rel="icon" type="image/png" href="{{URL::to('/')}}/admin/assets/images/icon.png">
    <link rel="stylesheet" type="text/css" href="{{URL::to('/')}}/admin/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{URL::to('/')}}/admin/assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{URL::to('/')}}/admin/assets/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="{{URL::to('/')}}/admin/assets/image-popup/source/jquery.fancybox.css?v=2.1.5" media="screen">
    <link rel="stylesheet" type="text/css" href="{{URL::to('/')}}/admin/assets/image-popup/source/helpers/jquery.fancybox-buttons.css?v=1.0.5">
    <link rel="stylesheet" type="text/css" href="{{URL::to('/')}}/admin/assets/css/style.css">
     @if($lang=="ar")
    <link rel="stylesheet" type="text/css" href="{{URL::to('/')}}/admin/assets/css/style-ar.css">
    @else
    <link rel="stylesheet" type="text/css" href="{{URL::to('/')}}/admin/assets/css/style-en.css">
    @endif
    @yield('contact_js')

</head>
