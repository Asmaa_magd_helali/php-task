@extends('site.interface.layouts.interface')

@section('content')

<!--===============================
    CONTENT
===================================-->

<div class="fixed-bg">
    <img src="{{URL::to('/')}}/admin/assets/images/1.jpg">
</div>


<div class="main-content">
    <div class="container-fluid">
        <h1 class="main-heading">{{trans('layout.depname')}}</h1>

        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                <a class="fancybox-buttons img-holder small-img" rel="gallery" title="" data-fancybox-group="button" href="{{URL::to('/')}}/admin/assets/images/1.jpg">
                    <img src="{{URL::to('/')}}/admin/assets/images/1.jpg" alt="img">
                </a>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                <a class="fancybox-buttons img-holder small-img" rel="gallery" title="" data-fancybox-group="button" href="{{URL::to('/')}}/admin/assets/images/2.jpg">
                    <img src="{{URL::to('/')}}/admin/assets/images/2.jpg" alt="img">
                </a>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                <a class="fancybox-buttons img-holder small-img" rel="gallery" title="" data-fancybox-group="button" href="{{URL::to('/')}}/admin/assets/images/3.jpg">
                    <img src="{{URL::to('/')}}/admin/assets/images/3.jpg" alt="img">
                </a>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                <a class="fancybox-buttons img-holder small-img" rel="gallery" title="" data-fancybox-group="button" href="{{URL::to('/')}}/admin/assets/images/1.jpg">
                    <img src="{{URL::to('/')}}/admin/assets/images/1.jpg" alt="img">
                </a>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                <a class="fancybox-buttons img-holder small-img" rel="gallery" title="" data-fancybox-group="button" href="{{URL::to('/')}}/admin/assets/images/2.jpg">
                    <img src="{{URL::to('/')}}/admin/assets/images/2.jpg" alt="img">
                </a>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                <a class="fancybox-buttons img-holder small-img" rel="gallery" title="" data-fancybox-group="button" href="{{URL::to('/')}}/admin/assets/images/3.jpg">
                    <img src="{{URL::to('/')}}/admin/assets/images/3.jpg" alt="img">
                </a>
            </div>
        </div>

    </div>
</div>

@stop
@section('category_js')

<script type="text/javascript" src="{{URL::to('/')}}/admin/assets/image-popup/source/jquery.fancybox.js?v=2.1.5"></script>
<script type="text/javascript" src="{{URL::to('/')}}/admin/assets/image-popup/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script>

        /*Button helper. Disable animations, hide close button, change title type and content*/

        $('.fancybox-buttons').fancybox({
            openEffect  : 'none',
            closeEffect : 'none',

            prevEffect : 'none',
            nextEffect : 'none',

            closeBtn  : false,

            helpers : {
                title : {
                    type : 'inside'
                },
                buttons	: {}
            },

            afterLoad : function() {
                this.title = '<a href="{{Share::load("http://www.example.com", "Photo Maker Studio")->facebook()}}" class="btn btn-fb btn-small"><i class="fa fa-facebook right-fa"></i> Share</a>' +
                        '<a href="{{Share::load("http://www.example.com", "Photo Maker Studio")->twitter()}}" class="btn btn-tw btn-small"><i class="fa fa-twitter right-fa"></i> Share</a>' +
                        '<a href="{{Share::load("http://www.example.com", "Photo Maker Studio")->gplus()}}" class="btn btn-inst btn-small"><i class="fa fa-google-plus right-fa"></i> Share</a>';
            }
        });


</script>
@stop