@extends('site.interface.layouts.interface')

@section('content')

<!--===============================
    CONTENT
===================================-->

<div class="fixed-bg">
    <img src="{{URL::to('/')}}/admin/assets/images/1.jpg">
</div>


<div class="main-content">
    <div class="container">
        <h1 class="main-heading">{{trans('layout.ourservices')}}</h1>


        <div class="border-bottom">

            <h1><strong>{{trans('layout.ourservices_div1_h1')}}</strong></h1>
            <p>{{trans('layout.ourservices_div1_p1')}}</p>
            <p>{{trans('layout.ourservices_div1_p2')}}</p>
            <ul class="list-numbered">
                <li> {{trans('layout.ourservices_div1_li1')}}</li>
                <li>{{trans('layout.ourservices_div1_li2')}}</li>
                <li>{{trans('layout.ourservices_div1_li3')}}</li>
            </ul>
        </div>

        <div class="border-bottom">
            <h1><strong>{{trans('layout.ourservices_div2_h1')}}</strong></h1>
            <p>{{trans('layout.ourservices_div2_p1')}}</p>
        </div>

        <div class="border-bottom">
            <h1><strong>{{trans('layout.ourservices_div3_h1')}}</strong></h1>
            <p>{{trans('layout.ourservices_div3_p1')}}</p>
            <ul class="list-numbered">
                <li>{{trans('layout.ourservices_div3_li1')}}</li>
                <li>{{trans('layout.ourservices_div3_li2')}}</li>
                <li>{{trans('layout.ourservices_div3_li3')}}</li>
            </ul>
        </div>

    </div>
</div>


@stop