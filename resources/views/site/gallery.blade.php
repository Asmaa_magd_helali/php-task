@extends('site.interface.layouts.interface')

@section('content')

<!--===============================
    CONTENT
===================================-->

<div class="fixed-bg">
    <img src="{{URL::to('/')}}/admin/assets/images/1.jpg">
</div>


<div class="main-content">
    <div class="container-fluid">
        <h1 class="main-heading">{{trans('layout.ourwork')}}</h1>

        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 no-padding">
                <a href="{{url('site/category')}}" class="img-holder">
                    <img src="{{URL::to('/')}}/admin/assets/images/1.jpg" alt="...">

                    <div class="hover-content">
                        <h1>{{trans('layout.depname')}}</h1>
                    </div>
                </a>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 no-padding">
                <a href="{{url('site/category')}}" class="img-holder">
                    <img src="{{URL::to('/')}}/admin/assets/images/2.jpg" alt="...">

                    <div class="hover-content">
                        <h1>{{trans('layout.depname')}}</h1>
                    </div>
                </a>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 no-padding">
                <a href="{{url('site/category')}}" class="img-holder">
                    <img src="{{URL::to('/')}}/admin/assets/images/3.jpg" alt="...">

                    <div class="hover-content">
                        <h1>{{trans('layout.depname')}}</h1>
                    </div>
                </a>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 no-padding">
                <a href="{{url('site/category')}}" class="img-holder">
                    <img src="{{URL::to('/')}}/admin/assets/images/1.jpg" alt="...">

                    <div class="hover-content">
                        <h1>{{trans('layout.depname')}}</h1>
                    </div>
                </a>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 no-padding">
                <a href="{{url('site/category')}}" class="img-holder">
                    <img src="{{URL::to('/')}}/admin/assets/images/2.jpg" alt="...">

                    <div class="hover-content">
                        <h1>{{trans('layout.depname')}}</h1>
                    </div>
                </a>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 no-padding">
                <a href="{{url('site/category')}}" class="img-holder">
                    <img src="{{URL::to('/')}}/admin/assets/images/3.jpg" alt="...">

                    <div class="hover-content">
                        <h1>{{trans('layout.depname')}}</h1>
                    </div>
                </a>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 no-padding">
                <a href="{{url('site/category')}}" class="img-holder">
                    <img src="{{URL::to('/')}}/admin/assets/images/1.jpg" alt="...">

                    <div class="hover-content">
                        <h1>{{trans('layout.depname')}}</h1>
                    </div>
                </a>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 no-padding">
                <a href="{{url('site/category')}}" class="img-holder">
                    <img src="{{URL::to('/')}}/admin/assets/images/2.jpg" alt="...">

                    <div class="hover-content">
                        <h1>{{trans('layout.depname')}}</h1>
                    </div>
                </a>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 no-padding">
                <a href="{{url('site/category')}}" class="img-holder">
                    <img src="{{URL::to('/')}}/admin/assets/images/3.jpg" alt="...">

                    <div class="hover-content">
                        <h1>{{trans('layout.depname')}}</h1>
                    </div>
                </a>
            </div>
        </div>

    </div>
</div>


@stop