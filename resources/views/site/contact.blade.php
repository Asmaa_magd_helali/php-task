@extends('site.interface.layouts.interface')
@section('contact_js')


<style>
    input[type="file"] {
        padding: 0;
    }

    .black-box.margin-bottom {
        margin: 0 0 15px;
    }

    .checkbox-holder {
        font-weight: 100;
        position: relative;
        cursor: pointer;
        margin-bottom: 10px;
        display: block;
    }

    .checkbox-holder span {
        vertical-align: middle;
    }

    .checkbox-holder .checkbox-icon {
        width: 13px;
        height: 13px;
        line-height: 7px;
        display: inline-block;
        border: 1px solid #000;
        background: #000;
        text-align: center;
        margin: 0 4px;
    }

    .checkbox-holder input[type="checkbox"] {
        position: absolute;
        opacity: 0;
        cursor: pointer;
    }

    .checkbox-holder .checkbox-icon:after {
        content: '';
        background: #000;
        width: 7px;
        height: 7px;
        display: block;
        margin: 2px;
    }

    .checkbox-holder input[type="checkbox"]:checked + .checkbox-icon {
        border-color: #00bcd4;
    }

    .checkbox-holder input[type="checkbox"]:checked + .checkbox-icon:after {
        background: #00bcd4;
    }

    .main-label {
        border-bottom: 1px dashed #00bcd4;
    }

    .check-open {
        margin-top: 10px;
    }
</style>
@stop


@section('content')
<div class="fixed-bg">
    <img src="{{URL::to('/')}}/admin/assets/images/1.jpg">
</div>


<div class="main-content">
    <div class="container">
        <h1 class="main-heading">{{trans('layout.contactus')}}</h1>

        <div class="row">
            <div class="col-xs-12 col-sm-8">
               {{Form::open(array('url'=>array('site/contactus') ,'method'=>'POST','enctypet'=>'multipart/from-data','files'=>'true'))}}
               {{ Form::text('company_name',null,['required','placeholder'=>trans('layout.company_name')]) }}
               {{ Form::text('activity_type',null,['required','placeholder'=>trans('layout.activity_type')]) }}
               {{ Form::number('telephone',null,['required','placeholder'=>trans('layout.telephone')]) }}
               {{ Form::email('contact_email',null,['required','placeholder'=>trans('layout.email')]) }}

               <label>{{trans('layout.service_type')}}</label>

               <div class="row">

                @forelse($services as $service)
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="box black-box margin-bottom">
                        <div class="main-label">
                            <label class="checkbox-holder">
                                <input type="checkbox">
                                <span class="checkbox-icon"></span>
                                <span>{{$service["title_$lang"]}}</span>
                            </label>
                        </div>

                        <div class="check-open">
                            @forelse($service->subServices as $subservice)
                            <label class="checkbox-holder">
                                <input type="checkbox" name="{{$service->id}}_sub_checkbox[]" value="{{$subservice->id}}">
                                <span class="checkbox-icon"></span>
                                <span>{{$subservice["title_$lang"]}}</span>
                            </label>
                            @empty
                            @endforelse
                            <label class="checkbox-holder">
                                <input type="checkbox">
                                <span class="checkbox-icon"></span>
                                <span> {{trans('layout.determine')}} </span>
                            </label>

                            <input type="text" value="" name="{{$service->id}}_other" placeholder="">

                            <label>{{trans('layout.number')}}</label>
                            <input type="number" value="0"
                            name="{{$service->id}}_number" placeholder="عدد الصور">
                            @forelse($service->serviceType as $servicetype)
                            <label class="checkbox-holder">
                                <input type="checkbox" name="{{$service->id}}_type_checkbox[]" value="{{$servicetype->id}}">
                                <span class="checkbox-icon"></span>
                                <span>{{$servicetype["title_$lang"]}}</span>
                            </label>
                            @empty
                            @endforelse
                        </div>
                    </div>
                </div>
                @empty
                @endforelse
            </div>




            <label>{{trans('layout.attach_file')}}</label>
            <input type="file" name="attachment" placeholder="{{trans('layout.attach_file')}}">
            <div class="btn btn-white btn-block">
                <span>{{ Form::submit(trans('layout.send')) }}</span>
            </div>
            {{Form::close()}}
        </div>

        <div class="col-xs-12 col-sm-4">
            <div class="box black-box text-center">
                <h3 class="main-heading">تفاصيل الاتصال</h3>

                <p><i class="fa fa-envelope-o right-fa"></i> Info@pmstu.com</p>
                <p><i class="fa fa-phone right-fa"></i> 0123456789</p>
            </div>
            <div class="box black-box text-center">
                <h3 class="main-heading">{{trans('layout.Join_us')}}</h3>
                {{Form::open(array('url'=>array('site/joinus') ,'method'=>'POST'))}}
                {{ Form::email('email',null,['placeholder'=>trans('layout.email')]) }}
                <font color="red">{{ $errors->first('email')}}</font>
                <div class="btn btn-white btn-block">
                    <span>{{ Form::submit(trans('layout.Join_us')) }}</span>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>'

</div>
</div>


@stop
@section('contact_footer_js')

<script>

    $('.check-open').slideUp(0);

    $('.main-label .checkbox-holder').click(function (){
        if($(this).find('input').is(':checked')) {
            $(this).parents('.main-label').next('.check-open').stop().slideDown();
        } else {
            $(this).parents('.main-label').next('.check-open').stop().slideUp();
        }
    });

</script>
@stop